# 大前端高薪训练营 014 期

> 大前端高薪训练营 014 期相关资料

点我 点我 点我 👉[大前端高薪训练营入营资料](prepare)

我们会随着学习计划，同步更新此仓库中对应的相关资料。

**作业模板:  https://gitee.com/lagoufed/lagoufed-e-task**

**常见问题:  https://gitee.com/lagoufed/fed-e-questions**

　

## 课程内容简介

​        我们的课程内容以录播+直播的形式，包括8个学习阶段，每个学习阶段有若干个模块，每个模块下有若干个任务。我们在模块任务结束后设有随堂测试，模块学习结束后设有模块作业。具体课程大纲参照[学习平台](https://edu.lagou.com)。
​      

## 学习平台使用

​      在我们的学习平台包括：课程资料、录播视频、模块任务随堂测试、模块作业、直播回放这几部分。详情移步[这里](prepare/use.md)。



## 仓库内容

- [面试题归档](interviews)
- [作业提交要求](prepare/requirements.md)
- [测试题](test)



## 意见或建议

如果你有什么好的意见和建议，可以随时通过 [Issues](https://gitee.com/lagoufed/fed-e-014/issues)  提出。

还是那句话 Issues 不是贴吧，请勿灌水 😄😄😄。



## 版权说明

&copy; 2021 [拉勾教育](https://kaiwu.lagou.com), 保留一切权利.

